#!/bin/bash

if [ $# == 0 ]; then
    echo "Usage: $0 desired-guacamole-password-here"
    exit 1
fi

if ! [ -x "$(command -v docker-compose)" ]; then
  if [ -x "$(command -v apt-get)" ]; then
    DEBIAN_FRONTEND=noninteractive apt-get install -yq docker.io docker-compose
  elif [ -x "$(command -v yum)" ]; then
    yum install -y yum-utils
    yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
    if [ $(cat /etc/centos-release | tr -dc '0-9.' | cut -d \. -f1) == "8" ]; then
      # CentOS 8
      yum install -y --nobest docker-ce docker-ce-cli containerd.io
      curl -L "https://github.com/docker/compose/releases/download/1.26.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
      chmod +x /usr/local/bin/docker-compose
      ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

    else
      # CentOS 7
      yum install -y docker-ce docker-ce-cli containerd.io docker-compose
    fi
  else
    exit 1
  fi
fi

systemctl enable docker
systemctl start docker

GUAC_PASSWORD_MD5=$(echo -n "$1" | md5sum | cut --fields=1 --only-delimited --delimiter=" ")

sed -i "s/{{ GUAC_PASSWORD_MD5 }}/$GUAC_PASSWORD_MD5/" config/user-mapping.xml

chmod 500 config/user-mapping.xml

# Not deploying database
# docker run --rm guacamole/guacamole /opt/guacamole/bin/initdb.sh --mysql > docker-entrypoint-initdb.d/initdb.sql

docker-compose up -d

echo '{"guacDeployComplete":true}' > /dev/console
